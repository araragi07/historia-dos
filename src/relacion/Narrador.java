/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacion;

/**
 *
 * @author bayer
 */
public class Narrador {
    int edad;
    void narracion1(){
       System.out.println("Era una noche lluviosa, cuando la conocio, o al menos de eso se convencio a si mismo en el primer momento que sus ojos se cruzaron bajo la lluvia ");
    }
    
    void narracion2(){
        System.out.println("el chico habia esperado la oportunidad para iniciar una conversacion apenas acabase la lluvia, pero ese hecho parecia cada vez mas lejano, asi que se lleno de valor y decidio comenzar una conversacion ");
    }
    void narracion3(){
        System.out.println("Sin que el chico se diese cuenta la conversacion se apago, tal como la luz de una fogata cuando esta apunto de llegar el amancer, y tan solo se oia alrededor el sordido sonido de la torrencial lluvia, trayendo consigo un ambiente un poco melancolico ");
    }
    void narracion4(){
        System.out.println("El joven decidio seguir con la conversacion, a pesar de que su corazon le imploraba que parara haciendole sufrir de una presion inusualmente agobiante como si la tierra misma lo aplastase ");
    }
    void narracion5(){
        System.out.println("Rapidamente la lluvia ceso, Anna se despidio con prisa, no sin antes mencionarme que habia disfrutado mucho el hablar conmigo finalizando con un gracias, no pude evitar el sonreir como idiota  ");
    }
    void narracion6(){
        System.out.println("nos separamos, pero antes de que saliese de mi campo de vision alze mi mano con impetú para despedirme, no recuerdo bien como llegue a casa pero si recuerdo que al siguiente dia, en la mañana desperte con lagrimas en los ojos, Anna fue mi mejor amiga y fallecio hace dos años el dia de su cumpleaños, y yo Matias por fin pude darle una merecida despedida ");
    }
}
